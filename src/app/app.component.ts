import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  algumTexto = 'Algum texto do component';

  umaFunc(){
    return 'Texto retornado da função...';
  }

  getVal(){
    return 3;
  }

  dtaNasc = new Date(2005, 2, 20);

  ehVerdade = true;
  msgOk = 'Tudo ok do component';

  ehFalso = 'falso';
  msgFalso = 'A mensagem do component falso';

  pessoas = [
    {
      nome: 'Pessoa 1',
      uf: 'PR'
    },
    {
      nome: 'Pessoa 2',
      uf: 'SC'
    },
    {
      nome: 'Pessoa 3',
      uf: 'RS'
    },
    {
      nome: 'Pessoa 4',
      uf: 'SP'
    },
    {
      nome: 'Pessoa 5',
      uf: 'RJ'
    },
    {
      nome: 'P  essoa 6',
      uf: 'MG'
    }
  ];

  ehEspecial = true;

  funcFormatDiv(){
    return { 
      'font-style' : 'bold',
      'font-family': 'Roboto',
      'font-size': '30px',
      'color' : 'blue'
    };
  }
}
